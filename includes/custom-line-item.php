<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
add_action( 'woocommerce_admin_order_item_headers', 'precificar_admin_order_items_headers', 10, 1 );
function precificar_admin_order_items_headers($order){
  ?>
  <th class="line_valor sortable" data-sort="float">
    Valor unitário
    <input type="hidden" id="order-value" value="<?php echo $order->get_id(); ?>">
  </th>
  
  <?php
}

add_action( 'woocommerce_admin_order_item_values', 'precificar_admin_order_item_values', 10, 3 );
function precificar_admin_order_item_values( $product, $item, $item_id ) {
  //Get what you need from $product, $item or $item_id
    $product = $item->get_product(); // Get the WC_Product Object
    $price   = $item->get_subtotal()/$item->get_quantity();
  ?>
  <td class="line_valor">
      <form>
        <span class="currencyinput">
          <?php echo get_woocommerce_currency_symbol() ?>
            <input id="<?php echo $item_id ?>" type="number" min="0" class="valor-novo" value="<?php echo $price; ?>">
        </span>
          
        
        <input type="hidden" min="0" class="id-item" value="<?php echo $item_id ?>">
        <button class="button button-primary precificar">Atualizar</span></button>
      </form>
  </td>
  <?php
}
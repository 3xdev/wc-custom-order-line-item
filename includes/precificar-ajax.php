<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
add_action( 'wp_ajax_woo_precificar', 'woo_do_precificar' );

function woo_do_precificar() {
   
    $pedido = $_POST['pedido'];//17
    $valor   =  $_POST['valor'];//125
    $item_id_recebido =  $_POST['item_id'];//4

    $order = new WC_Order( $pedido );
    $order_items = $order->get_items();

    // verifico aqui se os items no pedido tem os dados que eu enviei
    foreach ( $order_items as $key => $value ) {
        if ( $item_id_recebido == $key ) {
           $product_value = $value->get_data();
           $product_id    = $product_value['product_id'];
           $quantidade    = $value->get_quantity(); 
        }
    }
    $product = wc_get_product( $product_id );
    $price = $product->get_price();
    $price = ( int ) $quantidade * $valor;
    $order_items[ $item_id_recebido ]->set_quantity( $quantidade );
    $order_items[ $item_id_recebido ]->set_subtotal( $price );
    $order_items[ $item_id_recebido ]->set_total( $price );

    $order->calculate_taxes();
    $order->calculate_totals();
    $order->save();

    echo 'ok';
    wp_die(); // this is required to terminate immediately and return a proper response
}
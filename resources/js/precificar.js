// fill problems data
function precificar(pedido, valor, item_id){
    jQuery( '#woocommerce-order-items' ).block();
    var data = {
        'action': 'woo_precificar',
        'pedido': pedido,
        'valor' : valor,
        'item_id': item_id
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    var response = jQuery.post(ajaxurl, data, function(response) {
        
        if(response){
            // jQuery( '#woocommerce-order-items' ).unblock();
            console.log(response);
            // document.location.reload(true);
            jQuery( '#woocommerce-order-items' ).trigger( "wc_order_items_reload" );
        }   

    });    
}
jQuery(function() {
    jQuery( '#woocommerce-order-items' ).on( 'click', 'a.edit-order-item', function(){
        jQuery('.line_valor *').prop('disabled', true);
    });
    jQuery('body').on("click", ".precificar", function(e){
            e.preventDefault();
            var pedido = jQuery('#order-value').val();
            var valor = jQuery(this).parent().find('.valor-novo').val();
            var item_id = jQuery(this).parent().find('.id-item').val();

            precificar(pedido, valor, item_id);
    });
});
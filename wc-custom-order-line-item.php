<?php
/*
Plugin Name: Woo Precificar
Plugin URI: https://3xweb.site
Description: Customiza o formulário de itens na tela de edição de pedidos.
Version: 1.0
Author: Douglas de Araújo
Author URI: https://3web.site
License: Kopimi
*/
/**
 * Desativa o plugin caso não seja compatível.
 *
 * @since 1.0.0
 */
function deactivate_precificar_plugin(){
	// do_action( 'deactivated_plugin', __FILE__, false );
	deactivate_plugins( plugin_basename( __FILE__ ) );
	wp_die( 'Os requisitos míninos de funcionamento não foram atendidos, instale as depêndencias antes de prosseguir' );
	// die('Please Upgrade your WordPress to use this plugin.');
}
/**
 * WooCommerce fallback notice.
 *
 * @since 4.1.2
 */
function woo_precificar_missing_wc_notice() {
	/* translators: 1. URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Woo Precificar precisa que o WooCommerce esteja instalado e ativo. Você pode baixar o %s aqui.', 'woo-orderm-de-servico' ), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>' ) . '</strong></p></div>';
        deactivate_precificar_plugin();
}

function woo_precificar() {

	static $plugin;

	if ( ! isset( $plugin ) ) {

		class WC_PRECIFICAR {

			/**
			 * The *Singleton* instance of this class
			 *
			 * @var Singleton
			 */
			private static $instance;

			/**
			 * Returns the *Singleton* instance of this class.
			 *
			 * @return Singleton The *Singleton* instance.
			 */
			public static function get_instance() {
				if ( null === self::$instance ) {
					self::$instance = new self();
				}
				return self::$instance;
			}

			/**
			 * Private clone method to prevent cloning of the instance of the
			 * *Singleton* instance.
			 *
			 * @return void
			 */
			public function __clone() {}

			/**
			 * Private unserialize method to prevent unserializing of the *Singleton*
			 * instance.
			 *
			 * @return void
			 */
			public function __wakeup() {}

			/**
			 * Protected constructor to prevent creating a new instance of the
			 * *Singleton* via the `new` operator from outside of this class.
			 */
			public function __construct() {
				add_action( 'admin_init', [ $this, 'install' ] );

				if ( class_exists( 'WooCommerce' ) ) {
					if ( is_admin() ) {
						$this->admin_includes();
					}

					$this->includes();
					// add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
				} else {
					echo 'error loading this';
				}
			}
			/**
			 * Handles upgrade routines.
			 *
			 * @since 3.1.0
			 * @version 3.1.0
			 */
			public function install() {
				if ( ! is_plugin_active( plugin_basename( __FILE__ ) ) ) {
					return;
				}

			}
			private function includes() {}

			/**
			 * Admin includes.
			 */
			private function admin_includes() {
				include_once dirname( __FILE__ ) . '/includes/precificar-ajax.php';
                include_once dirname( __FILE__ ) . '/includes/custom-line-item.php';
			}

		}

		$plugin = WC_PRECIFICAR::get_instance();

	}

	return $plugin;
}
// carregando scripts adicionais
add_action( 'admin_enqueue_scripts', '_3xweb_precificar_scriptLoader' );
function _3xweb_precificar_scriptLoader(){
    // Register each script
    wp_register_script(
        'precificar_js', 
        plugins_url('/resources/js/precificar.js', __FILE__ ), 
        array( 'jquery' ),
        false,
        true
    );
	wp_register_style('precificar_css', plugins_url('/resources/css/estilo.css',__FILE__ ));
    
	wp_enqueue_script('precificar_js');
	wp_enqueue_style('precificar_css');
	
}

// iniciando o plugin e verificando as dependências
add_action( 'plugins_loaded', 'woo_precificar_init' );

function woo_precificar_init() {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'woo_precificar_missing_wc_notice' );
		return;
	}

	woo_precificar();
}